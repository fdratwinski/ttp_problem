import java.io.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class DataHolder {

    private static final Logger LOGGER = Logger.getLogger("DataHolder");
    private static String problemName;
    private static String knapscakDataType;
    private static double rentingRatio;
    private static String edgeWeightType;
    private static int numberOfDimension;
    private static int numberOfItems;
    private static int knapsackCapacity;
    private static double minSpeed;
    private static double maxSpeed;
    private static ArrayList<Town> towns;
    private static ArrayList<Item> items;
    private static PrintWriter CSVWriter;
    private static ArrayList<Item> evaluatedItemKNP;
    private static Map<Integer, Integer> townNumberToItemWeightSumMap;
    private static HashMap<Integer, HashMap<Integer, Double>> townDistances = new HashMap<>();

    public static void loadFromTTP(String nameOfFile) throws FileNotFoundException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream("data/" + nameOfFile)));
        String line;
        try {
            problemName = getNextData(bufferedReader);
            knapscakDataType = getNextData(bufferedReader);
            numberOfDimension = Integer.parseInt(getNextData(bufferedReader));
            numberOfItems = Integer.parseInt(getNextData(bufferedReader));
            knapsackCapacity = Integer.parseInt(getNextData(bufferedReader));
            minSpeed = Double.parseDouble(getNextData(bufferedReader));
            maxSpeed = Double.parseDouble(getNextData(bufferedReader));
            rentingRatio = Double.parseDouble(getNextData(bufferedReader));
            edgeWeightType = getNextData(bufferedReader);

            towns = new ArrayList<>(numberOfDimension);
            bufferedReader.readLine(); // This is done so unused line is read
            for (int i = 0; i < numberOfDimension; i++) {
                line = bufferedReader.readLine();
                String[] split = line.split("\t");
                towns.add(new Town(Integer.parseInt(split[0]),
                        Double.parseDouble(split[1]),
                        Double.parseDouble(split[2])));
            }
            bufferedReader.readLine();

            items = new ArrayList<>(numberOfItems);
            for (int i = 0; i < numberOfItems; i++) {
                line = bufferedReader.readLine();
                String[] split = line.split("\t");
                items.add(new Item(Integer.parseInt(split[0]),
                        Integer.parseInt(split[1]),
                        Integer.parseInt(split[2]),
                        Integer.parseInt(split[3])));
            }

            prepareData();
        } catch (IOException e) {
            LOGGER.severe("Cannot read from file: " + nameOfFile);
        }
    }

    private static String getNextData(BufferedReader bufferedReader) throws IOException {
        String line = bufferedReader.readLine();
        String[] piecesOfLine = line.replace("\t", " ").split(": ");
        return piecesOfLine[1].trim();
    }

    private static void prepareData() {
        evaluatedItemKNP = evaluateItemsKNP();
        townNumberToItemWeightSumMap = evaluateTownToItemWeightSumMap();
        townDistances = calculateDistances();
    }

    private static Map<Integer, Integer> evaluateTownToItemWeightSumMap() {
        Map<Integer, List<Item>> itemsByTownNumber = evaluatedItemKNP.stream().collect(Collectors.groupingBy(Item::getAssignedTown));
        Map<Integer, Integer> resultMap = new HashMap<>();
        itemsByTownNumber.forEach((town, items) -> resultMap.put(town, items.stream().mapToInt(Item::getWeight).sum()));
        return resultMap;
    }

    private static ArrayList<Item> evaluateItemsKNP() {
        ArrayList<Item> evaluatedItems = new ArrayList<>();
        int currentWeight = 0;
        Collections.sort(items);
        int i = 0;
        while (currentWeight < knapsackCapacity) {
            evaluatedItems.add(items.get(i));
            currentWeight += items.get(i).getWeight();
            i++;
        }
        evaluatedItems.remove(evaluatedItems.size() - 1);
        return evaluatedItems;
    }

    private static HashMap<Integer, HashMap<Integer, Double>> calculateDistances() {
        HashMap<Integer, HashMap<Integer, Double>> distances = new HashMap<>();
        for (Town first :
                towns) {
            HashMap<Integer, Double> integerDoubleHashMap = new HashMap<>();
            for (Town second :
                    towns) {
                if (first.getTownNumber() != second.getTownNumber()) {
                    integerDoubleHashMap.put(second.getTownNumber(), distanceBetween(first, second));
                }
            }
            distances.put(first.getTownNumber(), integerDoubleHashMap);
        }
        return distances;
    }
    private static double distanceBetween(Town firstTown, Town secondTown) {
        return Math.sqrt(Math.pow(firstTown.getCoordinateX() - secondTown.getCoordinateX(), 2) + Math.pow(firstTown.getCoordinateY() - secondTown.getCoordinateY(), 2));
    }

    public static void evaluate(Specimen specimen) {
        double fValue = 0;
        double gValue = 0;

        ArrayList<Town> townSequence = specimen.getTownSequence();
        ArrayList<Integer> specimenWeights = specimen.getWeights();

        for (int i = 0; i < townSequence.size() - 1; i++) {
            fValue += (townDistances.get(townSequence.get(i).getTownNumber()).get(townSequence.get((i + 1) % townSequence.size()).getTownNumber()) / calculateSpeedByWeight(specimenWeights.get(i)));
        }

        ArrayList<Item> finalItems = specimen.getFinalItems();
        for (Item finalItem : finalItems) {
            gValue += finalItem.getProfit();
        }
        specimen.setEvaluationValue(gValue - fValue);
    }

    private static double calculateSpeedByWeight(double specimenWeight) {
        return maxSpeed - (specimenWeight * ((maxSpeed - minSpeed) / knapsackCapacity));
    }


    private static double[] calculateValues(Specimen[] currentPopulation) {
        double[] values = new double[3];
        Arrays.sort(currentPopulation); //@TODO CHANGE TO SORT COPY
        values[0] = currentPopulation[0].getEvaluationValue();
        values[1] = currentPopulation[currentPopulation.length - 1].getEvaluationValue();
        double sum = 0;
        for (Specimen specimen : currentPopulation) {
            sum += specimen.getEvaluationValue();
        }
        values[2] = sum / currentPopulation.length;
        return values;
    }

    public static void writeToCSV(Specimen[] population, int generationNumber) {
        double[] values = calculateValues(population);
        List<String> stringValues = new ArrayList<>();
        stringValues.add(String.valueOf(generationNumber + 1));
        stringValues.add(String.valueOf(values[0]));
        stringValues.add(String.valueOf(values[2]));
        stringValues.add(String.valueOf(values[1]));
        try {
            writeLine(CSVWriter, stringValues);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeLine(Writer w, List<String> values) throws IOException {
        boolean first = true;

        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            value = value.replace(".", ",");
            if (!first) {
                sb.append(';');
            }
            sb.append(value);
            first = false;
        }
        sb.append("\n");
        w.append(sb.toString());
    }

    public static void writeFirstGenerationToCSV(Specimen[] population, String fileName) {
        try {
            CSVWriter = new PrintWriter(new FileWriter("wyniki/" + fileName + ".csv"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        CSVWriter.append("numer_populacji;best;avg;worst\n");
        double[] values = calculateValues(population);
        List<String> stringValues = new ArrayList<>();
        stringValues.add(String.valueOf(1));
        stringValues.add(String.valueOf(values[0]));
        stringValues.add(String.valueOf(values[2]));
        stringValues.add(String.valueOf(values[1]));
        try {
            writeLine(CSVWriter, stringValues);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void closePrintWriter() {
        CSVWriter.close();
    }

    public static Map<Integer, Integer> getTownNumberToItemWeightSumMap() {
        return townNumberToItemWeightSumMap;
    }

    public static ArrayList<Item> getEvaluatedItemKNP() {
        return evaluatedItemKNP;
    }

    public static ArrayList<Town> getTowns() {
        return towns;
    }

    public static ArrayList<Item> getItems() {
        return items;
    }

    public static int getKnapsackCapacity() {
        return knapsackCapacity;
    }

    public static double getMinSpeed() {
        return minSpeed;
    }

    public static double getMaxSpeed() {
        return maxSpeed;
    }
}
