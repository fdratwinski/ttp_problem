import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Specimen implements Comparable, Cloneable {

    private ArrayList<Town> townSequence;
    private ArrayList<Item> finalItems;
    private ArrayList<Integer> weights;
    private double evaluationValue;

    public Specimen(ArrayList<Town> townSequence) {
        this.townSequence = townSequence;
        finalItems = DataHolder.getEvaluatedItemKNP();
        weights = new ArrayList<Integer>(townSequence.size());
        calculateWeights();
    }

    public Specimen() {

    }

    private void calculateWeights() {
        Map<Integer, Integer> townNumberToItemWeightSum = DataHolder.getTownNumberToItemWeightSumMap();
        weights.clear();
        for (int i = 0; i < townSequence.size(); i++) {
            if (townNumberToItemWeightSum.get(townSequence.get(i).getTownNumber()) != null) {
                if (i != 0) {
                    weights.add(weights.get(i - 1) + townNumberToItemWeightSum.get(townSequence.get(i).getTownNumber()));
                } else {
                    weights.add(townNumberToItemWeightSum.get(townSequence.get(i).getTownNumber()));
                }
            } else if (i != 0) {
                weights.add(weights.get(i - 1));
            } else {
                weights.add(0);
            }
        }
    }

    public Specimen createOffspring(Specimen specimen) {
        ThreadLocalRandom rnd = ThreadLocalRandom.current();
        int startIndex = rnd.nextInt(townSequence.size());
        int endIndex;
        do {
            endIndex = rnd.nextInt(townSequence.size());
        } while (startIndex == endIndex);

        if (startIndex > endIndex) {
            int temp = startIndex;
            startIndex = endIndex;
            endIndex = temp;
        }

        Set<Integer> alreadyChosenTowns = new HashSet<Integer>();
        Town[] newTowns = new Town[townSequence.size()];
        int i = startIndex;
        for (; i < endIndex; i++) {
            newTowns[i] = this.townSequence.get(i);
            alreadyChosenTowns.add(newTowns[i].getTownNumber());
        }

        int freeIndex = 0;
        for (int j = 0; j < specimen.townSequence.size(); j++) {
            int index = (endIndex + j) % specimen.townSequence.size();
            int firstAvailable = (endIndex + freeIndex) % specimen.townSequence.size();

            if (!alreadyChosenTowns.contains(specimen.townSequence.get(index).getTownNumber())) {
                newTowns[firstAvailable] = specimen.townSequence.get(index);
                freeIndex++;
            }
        }
        ArrayList<Town> offspringTowns = new ArrayList<>(Arrays.asList(newTowns));
        return new Specimen(offspringTowns);
    }

    public void mutate() {
        ThreadLocalRandom rnd = ThreadLocalRandom.current();
        int firstIndex = rnd.nextInt(townSequence.size());
        int secondIndex;
        do {
            secondIndex = rnd.nextInt(townSequence.size());
        } while (firstIndex == secondIndex);

        Town tempTown = this.townSequence.get(firstIndex);
        this.townSequence.set(firstIndex, townSequence.get(secondIndex));
        this.townSequence.set(secondIndex, tempTown);
        calculateWeightsAfterMutation(firstIndex, secondIndex);
    }

    private void calculateWeightsAfterMutation(int firstIndex, int secondIndex) {
        if (firstIndex > secondIndex) {
            int temp = firstIndex;
            firstIndex = secondIndex;
            secondIndex = temp;
        }
        Map<Integer, Integer> townNumberToItemWeightSum = DataHolder.getTownNumberToItemWeightSumMap();
        for (int i = firstIndex; i < secondIndex; i++) {
            if (townNumberToItemWeightSum.get(townSequence.get(i).getTownNumber()) != null) {
                if (i != 0) {
                    weights.set(i, weights.get(i - 1) + townNumberToItemWeightSum.get(townSequence.get(i).getTownNumber()));
                } else {
                    weights.set(i, townNumberToItemWeightSum.get(townSequence.get(i).getTownNumber()));
                }
            } else if (i != 0) {
                weights.set(i, weights.get(i - 1));
            } else {
                weights.set(i, 0);
            }
        }
    }

    public ArrayList<Item> getFinalItems() {
        return finalItems;
    }

    public ArrayList<Town> getTownSequence() {
        return townSequence;
    }

    public ArrayList<Integer> getWeights() {
        return weights;
    }

    public double getEvaluationValue() {
        return evaluationValue;
    }

    public void setEvaluationValue(double evaluationValue) {
        this.evaluationValue = evaluationValue;
    }

    @Override
    public int compareTo(Object o) {
        return Double.compare(((Specimen) o).getEvaluationValue(), this.getEvaluationValue());
    }

    public Specimen clone() {
        Specimen clonedSpecimen = new Specimen();
        clonedSpecimen.finalItems = new ArrayList<>();
        clonedSpecimen.finalItems.addAll(this.finalItems);
        clonedSpecimen.evaluationValue = this.evaluationValue;
        clonedSpecimen.townSequence = new ArrayList<>(this.townSequence.size());
        clonedSpecimen.weights = new ArrayList<>(this.weights.size());
        clonedSpecimen.townSequence.addAll(this.townSequence);
        clonedSpecimen.weights.addAll(this.weights);
        return clonedSpecimen;
    }
}
