import java.io.FileNotFoundException;
import java.util.logging.Logger;

public class Main {

    private static final Logger LOGGER = Logger.getLogger("Main");
    private static final String FILE_NAME = "hard_0.ttp";
    private static final int POPULATION_SIZE = 1000;
    private static final int GENERATION_SIZE = 1000;
    private static final int TOURNAMENT_SIZE = 20;
    private static final int TOURNAMENT_CODE = 0;
    private static final int ROULETTE_CODE = 1;
    private static final double MUTATION_PROBABILITY = 0.2;
    private static final double CROSSOVER_PROBABILITY = 0.7;


    public static void main(String[] args) {
        try {
            DataHolder.loadFromTTP(FILE_NAME);
            PopulationManager populationManager = new PopulationManager(POPULATION_SIZE, GENERATION_SIZE, TOURNAMENT_SIZE, TOURNAMENT_CODE, MUTATION_PROBABILITY, CROSSOVER_PROBABILITY, "GA_turniej");
            populationManager.run();

//            PopulationManager populationManagerRoulette = new PopulationManager(POPULATION_SIZE, GENERATION_SIZE, TOURNAMENT_SIZE, ROULETTE_CODE, MUTATION_PROBABILITY, CROSSOVER_PROBABILITY, "GA_ruletka");
//            populationManagerRoulette.run();
//
//            RandomSearch randomSearch = new RandomSearch(POPULATION_SIZE, GENERATION_SIZE);
//            randomSearch.run();
//
//            NearestNeighbours nearestNeighbours = new NearestNeighbours();
//            nearestNeighbours.run();
        } catch (FileNotFoundException e) {
            LOGGER.severe("File with name: " + FILE_NAME + " has not been found");
        }

//        BufferedReader bw = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            bw.readLine();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
}
