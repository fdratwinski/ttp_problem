public class Town implements Cloneable{

    private int townNumber;
    private double coordinateX;
    private double coordinateY;

    public Town(int townNumber, double coordinateX, double coordinateY) {
        this.townNumber = townNumber;
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    public int getTownNumber() {
        return townNumber;
    }

    public double getCoordinateX() {
        return coordinateX;
    }

    public double getCoordinateY() {
        return coordinateY;
    }
}
