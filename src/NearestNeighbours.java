import java.util.*;

public class NearestNeighbours {

    private Specimen[] solutions;
    private HashMap<Integer, HashMap<Integer, Double>> distances = new HashMap<>();

    private void initializePopulation() {
        ArrayList<Town> towns = DataHolder.getTowns();
        solutions = new Specimen[towns.size()];

        for (int i = 0; i < solutions.length; i++) {
            solutions[i] = createSpecimen(towns, i);
        }
    }

    private Specimen createSpecimen(ArrayList<Town> towns, int i) {
        return new Specimen(calculateTowns(towns, i));
    }

    private ArrayList<Town> calculateTowns(ArrayList<Town> towns, int i) {
        ArrayList<Town> townSequence = new ArrayList<>();
        int currentTown = (i % distances.size()) + 1;
        Set<Integer> alreadyVisitedTowns = new HashSet<>();
        alreadyVisitedTowns.add(currentTown);
        townSequence.add(towns.get(currentTown - 1));
        HashMap<Integer, Double> distancesForCurrentTown;
        for (int j = 0; j < towns.size()-1; j++) {
            distancesForCurrentTown = distances.get(currentTown);
            double closest = Double.POSITIVE_INFINITY;
            int closestTown = 0;
            for (Map.Entry entry : distancesForCurrentTown.entrySet()) {
                if (!alreadyVisitedTowns.contains((Integer) entry.getKey()) && closest > (Double) entry.getValue()) {
                    closest = (double) entry.getValue();
                    closestTown = (int) entry.getKey();
                }
            }
            townSequence.add(towns.get(closestTown - 1));
            alreadyVisitedTowns.add(closestTown);
            currentTown = closestTown;
        }
        return townSequence;
    }

    public void run() {
        calculateDistances(DataHolder.getTowns());
        initializePopulation();
        evaulatePopulation();
        writeToCSV();
        closePrintWrite();
    }

    private void closePrintWrite() {
        DataHolder.closePrintWriter();
    }

    private void writeToCSV() {
        DataHolder.writeFirstGenerationToCSV(solutions, "nearest_neighbours");
    }

    private void evaulatePopulation() {
        for (int i = 0; i < solutions.length; i++) {
            DataHolder.evaluate(solutions[i]);
        }
    }

    private void calculateDistances(ArrayList<Town> towns) {
        for (Town first :
                towns) {
            HashMap<Integer, Double> integerDoubleHashMap = new HashMap<>();
            for (Town second :
                    towns) {
                if (first.getTownNumber() != second.getTownNumber()) {
                    integerDoubleHashMap.put(second.getTownNumber(), distanceBetween(first, second));
                }

            }
            distances.put(first.getTownNumber(), integerDoubleHashMap);
        }
    }

    private double distanceBetween(Town firstTown, Town secondTown) {
        return Math.sqrt(Math.pow(firstTown.getCoordinateX() - secondTown.getCoordinateX(), 2) + Math.pow(firstTown.getCoordinateY() - secondTown.getCoordinateY(), 2));
    }

}
