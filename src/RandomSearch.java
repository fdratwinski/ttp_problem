import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

public class RandomSearch {

    private Logger LOGGER = Logger.getLogger("PopulationManager");
    private Specimen[] solutions;
    private int populationSize;
    private int generationSize;
    private ThreadLocalRandom rnd = ThreadLocalRandom.current();

    public RandomSearch(int populationSize, int generationSize) {
        this.populationSize = populationSize;
        this.generationSize = generationSize;
    }

    private void initializeSolutions() {
        LOGGER.info("Initializing solutions...");

        ArrayList<Town> towns = DataHolder.getTowns();
        solutions = new Specimen[populationSize];

        for (int i = 0; i < populationSize; i++) {
            solutions[i] = randomizeSpecimen(towns);
        }

        LOGGER.info("Initialized solutions. Size of solutions: " + populationSize);
    }

    private Specimen randomizeSpecimen(ArrayList<Town> towns) {
        return new Specimen(randomizeTowns(towns));
    }

    private ArrayList<Town> randomizeTowns(ArrayList<Town> towns) {
        ArrayList<Town> randomizedTowns = new ArrayList<>(towns.size());
        randomizedTowns.addAll(towns);
        for (int i = 0; i < towns.size(); i++) {
            int index = rnd.nextInt(i + 1);
            Town town = randomizedTowns.get(index);
            randomizedTowns.set(index, randomizedTowns.get(i));
            randomizedTowns.set(i, town);
        }
        return randomizedTowns;
    }

    public void run() {
        int iteration = 0;
        initializeSolutions();
        evaulateSolutions();
        iteration++;
        writeFirstIterationToCSV();
        for (; iteration < generationSize; iteration++) {
            initializeSolutions();
            evaulateSolutions();
            writeIterationToCSV(iteration);
            LOGGER.info("ITERATION ENDED. NUMBER OF ITERATION: " + iteration);
        }
        closePrintWrite();
    }

    private void closePrintWrite() {
        DataHolder.closePrintWriter();
    }

    private void writeFirstIterationToCSV() {
        DataHolder.writeFirstGenerationToCSV(solutions, "random_search");
    }

    private void writeIterationToCSV(int generation) {
        DataHolder.writeToCSV(solutions, generation);
    }

    private void evaulateSolutions() {
        LOGGER.info("Evaluating solutions...");
        for (int i = 0; i < populationSize; i++) {
            DataHolder.evaluate(solutions[i]);
        }
        LOGGER.info("Evaluated solutions.");
    }

}
