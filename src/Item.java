public class Item implements Comparable {

    private int itemNumber;
    private int profit;
    private int weight;
    private double ratio;
    private int assignedTown;

    public Item(int itemNumber, int profit, int weight, int assignedTown) {
        this.itemNumber = itemNumber;
        this.profit = profit;
        this.weight = weight;
        this.assignedTown = assignedTown;
        ratio = (double) profit / weight;
    }

    public double getRatio() {
        return ratio;
    }

    public int getProfit() {
        return profit;
    }

    public int getWeight() {
        return weight;
    }

    public int getAssignedTown() {
        return assignedTown;
    }

    @Override
    public int compareTo(Object object) {
        Item item = (Item) object;
        return Double.compare(item.getRatio(), ratio);
    }
}
