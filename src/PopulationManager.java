import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

public class PopulationManager {

    private Logger LOGGER = Logger.getLogger("PopulationManager");
    private Specimen[] population;
    private int populationSize;
    private int tournamentSize;
    private int generationSize;
    private int tournamentType;
    private double mutationProbability;
    private double crossoverProbability;
    private String fileName;
    private Specimen previousBest;
    private ThreadLocalRandom rnd = ThreadLocalRandom.current();

    public PopulationManager(int populationSize, int generationSize, int tournamentSize, int tournamentType, double mutationProbability, double crossoverProbability, String fileName) {
        this.populationSize = populationSize;
        this.generationSize = generationSize;
        this.tournamentSize = tournamentSize;
        this.tournamentType = tournamentType;
        this.mutationProbability = mutationProbability;
        this.crossoverProbability = crossoverProbability;
        this.fileName = fileName;
    }

    private void initializePopulation() {
        LOGGER.info("Initializing population...");

        ArrayList<Town> towns = DataHolder.getTowns();
        population = new Specimen[populationSize];

        for (int i = 0; i < populationSize; i++) {
            population[i] = randomizeSpecimen(towns);
        }

        LOGGER.info("Initialized population. Size of population: " + populationSize);
    }

    private Specimen randomizeSpecimen(ArrayList<Town> towns) {
        return new Specimen(randomizeTowns(towns));
    }

    private ArrayList<Town> randomizeTowns(ArrayList<Town> towns) {
        ArrayList<Town> randomizedTowns = new ArrayList<>(towns.size());
        randomizedTowns.addAll(towns);
        for (int i = 0; i < towns.size(); i++) {
            int index = rnd.nextInt(i + 1);
            Town town = randomizedTowns.get(index);
            randomizedTowns.set(index, randomizedTowns.get(i));
            randomizedTowns.set(i, town);
        }
        return randomizedTowns;
    }

    public void run() {
        int generation = 0;
        initializePopulation();
        evaulatePopulation();
        generation++;
        writeFirstGenerationToCSV();
        for (; generation < generationSize; generation++) {
            selectionPopulation();
            crossoverPopulation();
            mutatePopulation();
            evaulatePopulation();
//            storeBest();
            writeGenerationToCSV(generation);
            LOGGER.info("GENERATION ENDED. NUMBER OF GENERATION: " + generation);
        }
        closePrintWrite();
    }

    private void closePrintWrite() {
        DataHolder.closePrintWriter();
    }

    private void writeFirstGenerationToCSV() {
        DataHolder.writeFirstGenerationToCSV(population, fileName);
    }

    private void storeBest() {
        double worst = Double.POSITIVE_INFINITY;
        int worstIndex = 0;
        double best = Double.NEGATIVE_INFINITY;
        int bestIndex = 0;
        for (int i = 0; i < population.length; i++) {
            if (population[i].getEvaluationValue() < worst) {
                worst = population[i].getEvaluationValue();
                worstIndex = i;
            }
            if (population[i].getEvaluationValue() > best) {
                best = population[i].getEvaluationValue();
                bestIndex = i;
            }
        }

        if (previousBest != null) {
            if (previousBest.getEvaluationValue() >= best) {
                population[worstIndex] = previousBest.clone();
            } else {
                previousBest = population[bestIndex].clone();
            }
        } else {
            previousBest = population[bestIndex].clone();
        }
    }

    private void writeGenerationToCSV(int generation) {
        DataHolder.writeToCSV(population, generation);
    }

    private void mutatePopulation() {
        for (int i = 0; i < populationSize; i++) {
            if (rnd.nextDouble(1) < mutationProbability) {
                population[i].mutate();
            }
        }
    }

    private void crossoverPopulation() {
        for (int i = 0; i < populationSize; i++) {
            int randomSpecimenNumber = rnd.nextInt(populationSize - 1);
            while (randomSpecimenNumber == i) {
                randomSpecimenNumber = rnd.nextInt(populationSize - 1);
            }
            if (rnd.nextDouble(1) < crossoverProbability) {
                Specimen firstOffspring = population[i].createOffspring(population[randomSpecimenNumber]);
                Specimen secondOffSpring = population[randomSpecimenNumber].createOffspring(population[i]);
                population[i] = firstOffspring;
                population[randomSpecimenNumber] = secondOffSpring;
            }
        }
    }

    private void evaulatePopulation() {
        for (int i = 0; i < populationSize; i++) {
            DataHolder.evaluate(population[i]);
        }
    }

    private void selectionPopulation() {
        if (tournamentType == 0) {
            tournament();
        } else {
            roulette();
        }
    }

    private void tournament() {
        Specimen[] newPopulation = new Specimen[population.length];
        int i = 0;
        while (i < populationSize) {
            int bestSpecimenIndex = bestInTournament();
            newPopulation[i] = population[bestSpecimenIndex].clone();
            i++;
        }
        population = newPopulation;
    }

    private int bestInTournament() {
        Set<Integer> randomSpecimenNumbers = new HashSet<>();
        randomSpecimenNumbers.clear();
        while (randomSpecimenNumbers.size() < tournamentSize) {
            randomSpecimenNumbers.add((int) Math.round(Math.random() * (populationSize - 1)));
        }

        int bestSpecimen = 0;
        double bestValue = Double.NEGATIVE_INFINITY;
        for (Integer specimenNumber : randomSpecimenNumbers) {
            if (population[specimenNumber].getEvaluationValue() > bestValue) {
                bestSpecimen = specimenNumber;
                bestValue = population[specimenNumber].getEvaluationValue();
            }
        }
        return bestSpecimen;
    }

    private void roulette() {
        double sumOfEvaluationValues = 0;

        double worst = Double.POSITIVE_INFINITY;
        for (Specimen specimen1 : population) {
            if (specimen1.getEvaluationValue() < worst) {
                worst = specimen1.getEvaluationValue();
            }
        }

        for (Specimen specimen : population) {
            sumOfEvaluationValues += (specimen.getEvaluationValue() - worst + 1);
        }

        double[] probabilities = new double[population.length + 1];
        double sumOfProbabilities = 0;
        probabilities[0] = 0;
        for (int i = 0; i < population.length; i++) {
            probabilities[i + 1] = sumOfProbabilities + ((population[i].getEvaluationValue() - worst + 1) / sumOfEvaluationValues);
            sumOfProbabilities += ((population[i].getEvaluationValue() - worst + 1) / sumOfEvaluationValues);
        }

        Specimen[] newPopulation = new Specimen[population.length];
        int i = 0;
        while (i < populationSize) {
            int specimenIndex = rollTheRoulette(population, probabilities);
            newPopulation[i] = population[specimenIndex].clone();
            i++;
        }
        population = newPopulation;
    }

    private int rollTheRoulette(Specimen[] currentPopulation, double[] probabilities) {
        double number = rnd.nextDouble(1);
        for (int i = 0; i < currentPopulation.length; i++) {
            if (number >= probabilities[i] && number <= probabilities[i + 1]) {
                return i;
            }
        }
        return -1;
    }
}
